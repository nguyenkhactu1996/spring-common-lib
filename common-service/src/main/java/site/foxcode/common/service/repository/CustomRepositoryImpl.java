package site.foxcode.common.service.repository;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import java.io.Serializable;

public class CustomRepositoryImpl <T, I extends Serializable> extends SimpleJpaRepository<T, I> implements CustomRepository<T, I>{
    private final EntityManager entityManager;

    public CustomRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    public void clear() {
        this.entityManager.clear();
    }

    @Transactional
    public void refresh(T t) {
        this.entityManager.refresh(t);
    }

    public T findByIdWithLock(I i, Class<T> c) {
        return this.entityManager.find(c, i, LockModeType.PESSIMISTIC_WRITE);
    }
}
