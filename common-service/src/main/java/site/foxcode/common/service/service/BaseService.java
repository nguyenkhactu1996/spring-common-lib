package site.foxcode.common.service.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import site.foxcode.common.service.entity.BaseEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface BaseService<T extends BaseEntity<I>, I extends Serializable> {
    T create(T entity);

    T createNow(T entity);

    Optional<T> update(I id, T entity);

    Optional<T> updateNow(I id, T entity);

    void delete(I id);

    void delete(T entity);

    T find(String filter);

    Page<T> find(Pageable pageable);

    Page<T> find(String filter, Pageable pageable);

    T findById(I id);

    T findByIdWithLock(I id, Class<T> c);

    T findOneByField(String field, Object value);

    List<T> findAll();

    List<T> findByIds(List<I> ids);

    Page<T> findByIds(List<I> ids, Pageable pageable);

    List<T> findByField(String field, Object value);

    List<T> findByField(String field, List<Object> values);

    Page<T> findByField(String field, Object values, Pageable pageable);

    long count();
}
