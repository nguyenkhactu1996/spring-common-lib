package site.foxcode.common.service.service.impl;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import site.foxcode.common.service.entity.BaseEntity;
import site.foxcode.common.service.service.BaseService;
import site.foxcode.elasticsearch.ElasticSearchTransportClient;
import site.foxcode.elasticsearch.SpecificationsBuilder;
import site.foxcode.util.exception.ExceptionUtil;
import site.foxcode.util.mapper.MapperUtil;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class BaseServiceEsImpl <T extends BaseEntity<I>, I extends Serializable> implements BaseService<T, I> {
    private static final Logger log = LoggerFactory.getLogger(BaseServiceEsImpl.class);
    @Value("${application.common-service.enable.scroll:false}")
    private Boolean enableScroll;

    private final ElasticSearchTransportClient transport;
    private final SpecificationsBuilder specificationsBuilder;


    public BaseServiceEsImpl(ElasticSearchTransportClient transport, SpecificationsBuilder specificationsBuilder) {
        this.transport = transport;
        this.specificationsBuilder = specificationsBuilder;
    }

    protected abstract String getIndex();

    protected abstract Class<T> getClassType();

    @Override
    public T create(T entity) {
        log.info("(create)index: {}, object: {}", this.getIndex(), entity);
        try{
            IndexRequest request = new IndexRequest(this.getIndex());
            request.id((String) entity.getId());
            request.source(MapperUtil.toJson(entity), XContentType.JSON);
            this.transport.getClient().index(request, RequestOptions.DEFAULT);
            return entity;
        }catch (Exception e){
            log.error("(create)index: {}, object: {}, ex: {}", this.getIndex(), entity, ExceptionUtil.getFullStackTrace(e));
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public T createNow(T entity) {
        log.info("(createNow)index: {}, object: {}", this.getIndex(), entity);
        return this.create(entity);
    }

    @Override
    public Optional<T> update(I id, T entity) {
        log.info("(update)index: {}, object: {}", this.getIndex(), entity);
        try{
            UpdateRequest updateRequest = new UpdateRequest(this.getIndex(), id.toString());
            updateRequest.doc(MapperUtil.toJson(entity), XContentType.JSON);
            this.transport.getClient().update(updateRequest, RequestOptions.DEFAULT);
            return Optional.of(entity);
        }catch (Exception e){
            log.error("(update)index: {}, object: {}, id: {}, ex: {}", this.getIndex(), entity, ExceptionUtil.getFullStackTrace(e));
        }
        return Optional.empty();
    }

    public void update(String id, String field, Object value) {
        log.info("(update)index: {}, id: {}, field: {}, value: {}", new Object[]{this.getIndex(), id, field, value});

        try {
            Map<String, Object> jsonMap = new HashMap();
            jsonMap.put(field, value);
            UpdateRequest request = (new UpdateRequest(this.getIndex(), id)).doc(jsonMap);
            this.transport.getClient().update(request, RequestOptions.DEFAULT);
        } catch (IOException var6) {
            log.error("(update)index: {}, id: {}, field: {}, value: {}, ex: {}", new Object[]{this.getIndex(), id, field, value, ExceptionUtil.getFullStackTrace(var6)});
            throw new RuntimeException(var6.getMessage());
        }
    }

    @Override
    public Optional<T> updateNow(I id, T entity) {
        log.info("(updateNow)index: {}, object: {}", this.getIndex(), entity);
        return this.update(id, entity);
    }

    @Override
    public void delete(I id) {
        log.info("(delete)index: {}, id: {}", this.getIndex(), id);

        try {
            DeleteRequest request = new DeleteRequest(this.getIndex(), id.toString());
            this.transport.getClient().delete(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("(delete)index: {}, id: {}, ex: {}", this.getIndex(), id, ExceptionUtil.getFullStackTrace(e));
        }
    }

    @Override
    public void delete(T entity) {

    }

    @Override
    public T find(String filter) {
        return null;
    }

    @Override
    public Page<T> find(Pageable pageable) {
        return null;
    }

    @Override
    public Page<T> find(String filter, Pageable pageable) {
        return null;
    }

    @Override
    public T findById(I id) {
        return null;
    }

    @Override
    public T findByIdWithLock(I id, Class<T> c) {
        return null;
    }

    @Override
    public T findOneByField(String field, Object value) {
        return null;
    }

    @Override
    public List<T> findAll() {
        return null;
    }

    @Override
    public List<T> findByIds(List<I> ids) {
        return null;
    }

    @Override
    public Page<T> findByIds(List<I> ids, Pageable pageable) {
        return null;
    }

    @Override
    public List<T> findByField(String field, Object value) {
        return null;
    }

    @Override
    public List<T> findByField(String field, List<Object> values) {
        return null;
    }

    @Override
    public Page<T> findByField(String field, Object values, Pageable pageable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    public Page<T> search(QueryBuilder query, SortBuilder sort, Pageable pageable) {
        try {
            if (this.enableScroll) {
                return this.searchScroll(query, sort, pageable);
            } else {
                log.info("(search)index: {}, query: {}, sort: {}, pageable: {}", new Object[]{this.getIndex(), query, sort, pageable});
                List<T> data = new ArrayList();
                SearchRequest request = new SearchRequest(new String[]{this.getIndex()});
                SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
                searchSourceBuilder.query(query);
                searchSourceBuilder.size(pageable.getPageSize());
                searchSourceBuilder.from(pageable.getPageSize() * pageable.getPageNumber());
                searchSourceBuilder.trackTotalHits(true);
                if (sort != null) {
                    searchSourceBuilder.sort(sort);
                }

                request.source(searchSourceBuilder);
                SearchResponse searchResponse = this.transport.getClient().search(request, RequestOptions.DEFAULT);
                SearchHit[] var8 = searchResponse.getHits().getHits();
                int var9 = var8.length;

                for(int var10 = 0; var10 < var9; ++var10) {
                    SearchHit searchHit = var8[var10];
                    data.add(MapperUtil.getMapper().readValue(searchHit.getSourceAsString(), this.getClassType()));
                }

                return new PageImpl(data, pageable, searchResponse.getHits().getTotalHits().value);
            }
        } catch (Exception var12) {
            log.error("(search)index: {}, queryBuilder: {}, pageable: {}, ex: {}", new Object[]{this.getIndex(), query, pageable, ExceptionUtil.getFullStackTrace(var12)});
            return Page.empty();
        }
    }

    private Page<T> searchScroll(QueryBuilder queryBuilder, SortBuilder sort, Pageable pageable) {
        try {
            Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
            SearchRequest request = new SearchRequest(new String[]{this.getIndex()});
            request.scroll(scroll);
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(queryBuilder);
            searchSourceBuilder.size(pageable.getPageSize());
            searchSourceBuilder.from(pageable.getPageSize() * pageable.getPageNumber());
            if (sort != null) {
                searchSourceBuilder.sort(sort);
            }

            request.source(searchSourceBuilder);
            SearchResponse searchResponse = this.transport.getClient().search(request, RequestOptions.DEFAULT);
            String scrollId = searchResponse.getScrollId();
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            List<T> data = new ArrayList();

            long total;
            for(total = 0L; searchHits != null && searchHits.length > 0; searchHits = searchResponse.getHits().getHits()) {
                total += searchResponse.getHits().getTotalHits().value;
                log.debug("(find)index: {}, total: {}, --> SCROLL", this.getIndex(), total);
                SearchHit[] var13 = searchResponse.getHits().getHits();
                int var14 = var13.length;

                for(int var15 = 0; var15 < var14; ++var15) {
                    SearchHit searchHit = var13[var15];
                    data.add(MapperUtil.getMapper().readValue(searchHit.getSourceAsString(), this.getClassType()));
                }

                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = this.transport.getClient().scroll(scrollRequest, RequestOptions.DEFAULT);
                scrollId = searchResponse.getScrollId();
            }

            ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
            clearScrollRequest.addScrollId(scrollId);
            ClearScrollResponse clearScrollResponse = this.transport.getClient().clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
            boolean succeeded = clearScrollResponse.isSucceeded();
            log.debug("(find)index: {}, total: {}, clear scroll: {}", new Object[]{this.getIndex(), total, succeeded});
            return new PageImpl(data, pageable, total);
        } catch (Exception var17) {
            log.error("(searchScroll)index: {}, queryBuilder: {}, pageable: {}, ex: {}", new Object[]{this.getIndex(), queryBuilder, pageable, ExceptionUtil.getFullStackTrace(var17)});
            return Page.empty();
        }
    }
}
