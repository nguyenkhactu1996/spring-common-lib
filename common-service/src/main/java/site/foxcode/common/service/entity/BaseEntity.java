package site.foxcode.common.service.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@EntityListeners({AuditingEntityListener.class})
@MappedSuperclass
public class BaseEntity<I extends Serializable> {
    @Id
    @Column(name = "id")
    private I id;

    public I getId() {
        return id;
    }

    public void setId(I id) {
        this.id = id;
    }
}
