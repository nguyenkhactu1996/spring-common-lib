package site.foxcode.common.service.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import site.foxcode.common.service.entity.BaseEntity;
import site.foxcode.common.service.repository.CustomRepository;
import site.foxcode.common.service.service.BaseService;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class BaseServiceSqlImpl<T extends BaseEntity<I>, I extends Serializable> implements BaseService<T, I> {
    private static final Logger log = LoggerFactory.getLogger(BaseServiceSqlImpl.class);
    public abstract CustomRepository<T, I> getRepository();
    @Override
    public T create(T entity) {
        log.info("(create)type: {}, object: {}", entity.getClass(), entity);
        return this.getRepository().save(entity);
    }

    @Override
    public T createNow(T entity) {
        log.info("(createNow)type: {}, object: {}", entity.getClass(), entity);
        return this.getRepository().saveAndFlush(entity);
    }

    @Override
    public Optional<T> update(I id, T entity) {
        log.info("(update)id: {}, type: {}, object: {}", new Object[]{id, entity.getClass(), entity});
        return this.getRepository().findById(id).map(oldEntity -> {
            return this.getRepository().save(entity);
        });
    }

    @Override
    public Optional<T> updateNow(I id, T entity) {
        log.info("(updateNow)id: {}, type: {}, object: {}", new Object[]{id, entity.getClass(), entity});
        return this.getRepository().findById(id).map(oldEntity -> {
            return this.getRepository().saveAndFlush(entity);
        });
    }

    @Override
    public void delete(I id) {
        log.info("(delete)id: {}", id);
        T entity = this.findById(id);
        this.delete(entity);
    }

    @Override
    public void delete(T entity) {
        this.getRepository().delete(entity);
    }

    @Override
    public T find(String filter) {
        log.info("(find)filter: {}", filter);
        Page<T> results = this.find(filter, PageRequest.of(0, 1));
        return results.hasContent() ? results.getContent().get(0) : null;
    }

    @Override
    public Page<T> find(Pageable pageable) {
        log.info("(find)pageable: {}", pageable);
        return this.getRepository().findAll(pageable);
    }

    @Override
    public Page<T> find(String filter, Pageable pageable) {
        return null;
    }

    @Override
    public T findById(I id) {
        return null;
    }

    @Override
    public T findByIdWithLock(I id, Class<T> c) {
        return null;
    }

    @Override
    public T findOneByField(String field, Object value) {
        return null;
    }

    @Override
    public List<T> findAll() {
        return null;
    }

    @Override
    public List<T> findByIds(List<I> ids) {
        return null;
    }

    @Override
    public Page<T> findByIds(List<I> ids, Pageable pageable) {
        return null;
    }

    @Override
    public List<T> findByField(String field, Object value) {
        return null;
    }

    @Override
    public List<T> findByField(String field, List<Object> values) {
        return null;
    }

    @Override
    public Page<T> findByField(String field, Object values, Pageable pageable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }
}
