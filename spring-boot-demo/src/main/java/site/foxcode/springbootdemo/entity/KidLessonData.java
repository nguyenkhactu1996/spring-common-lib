package site.foxcode.springbootdemo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import site.foxcode.common.service.entity.BaseEntity;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KidLessonData extends BaseEntity<String> {

    private String _class;

    private String name;

    @JsonProperty("name_vi")
    private String nameVi;

    private String description;

    @JsonProperty("unit_id")
    private Object unitId;

    @JsonProperty("is_active")
    private Short isActive;

    @JsonProperty("slug")
    private String slug;

    @JsonProperty("img_learned")
    private String imgLearned;

    @JsonProperty("img_n_learned")
    private String imgNLearned;

    @JsonProperty("lesson_code")
    private String lessonCode;

    @JsonProperty("position")
    private Integer position;

    @JsonProperty("sticker")
    private Integer sticker;

    @JsonProperty("type")
    private Short type;

    @JsonProperty("sort_level")
    private Integer sortLevel;

    @JsonProperty("timestamp")
    private BigInteger timestamp;

    @JsonProperty("created_at")
    private Long createdAt;

    @JsonProperty("updated_at")
    private Long updatedAt;
}
