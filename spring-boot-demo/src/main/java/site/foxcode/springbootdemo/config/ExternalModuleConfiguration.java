package site.foxcode.springbootdemo.config;

import org.springframework.context.annotation.Configuration;
import site.foxcode.elasticsearch.EnableCoreEs;

@Configuration
@EnableCoreEs
public class ExternalModuleConfiguration {

}
