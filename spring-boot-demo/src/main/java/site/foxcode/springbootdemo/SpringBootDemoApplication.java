package site.foxcode.springbootdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.data.domain.Page;
import site.foxcode.elasticsearch.ElasticSearchTransportClient;
import site.foxcode.springbootdemo.dto.KidLessonFilterDTO;
import site.foxcode.springbootdemo.entity.KidLessonData;
import site.foxcode.springbootdemo.entity.UserEntity;
import site.foxcode.springbootdemo.service.UserService;
import site.foxcode.springbootdemo.service.es.KidLessonDataService;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.UUID;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class SpringBootDemoApplication implements CommandLineRunner {
    private final UserService userService;
    private final ElasticSearchTransportClient transportClient;
    private final KidLessonDataService kidLessonDataService;


    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Command is running ....");
//        testEs();
        KidLessonFilterDTO filterDTO = new KidLessonFilterDTO();
        filterDTO.setId("96");
        Page<KidLessonData> data = kidLessonDataService.find(filterDTO);
        log.info("size: [{}]", data.getTotalElements());
    }

    private void testEs() throws IOException {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.termQuery("id", "96"));

        SearchRequest request = new SearchRequest("kid_lesson");

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(queryBuilder);
        searchSourceBuilder.trackTotalHits(true);
        request.source(searchSourceBuilder);

        try(RestHighLevelClient client = this.transportClient.getClient()){
            SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            log.info("Total hit response: [{}]", searchHits.length);
            for(SearchHit searchHit: searchHits){
                log.info("[{}]", searchHit.getSourceAsString());
                KidLessonData kidLessonData = new ObjectMapper().readValue(searchHit.getSourceAsString(), KidLessonData.class);
                log.info("Lesson name: [{}]", kidLessonData.getName());
                log.info("Created at: [{}]", Instant.ofEpochMilli(kidLessonData.getCreatedAt()).atZone(ZoneId.systemDefault()).toLocalDateTime());
            }
        }
    }
}
