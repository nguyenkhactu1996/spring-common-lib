package site.foxcode.springbootdemo.service;

import site.foxcode.common.service.service.BaseService;
import site.foxcode.springbootdemo.entity.UserEntity;

public interface UserService extends BaseService<UserEntity, String> {

}
