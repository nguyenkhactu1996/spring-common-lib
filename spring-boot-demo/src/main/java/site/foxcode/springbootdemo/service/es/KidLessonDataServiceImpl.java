package site.foxcode.springbootdemo.service.es;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import site.foxcode.common.service.service.impl.BaseServiceEsImpl;
import site.foxcode.elasticsearch.ElasticSearchTransportClient;
import site.foxcode.elasticsearch.SpecificationsBuilder;
import site.foxcode.springbootdemo.dto.KidLessonFilterDTO;
import site.foxcode.springbootdemo.entity.KidLessonData;

@Service
@Slf4j
public class KidLessonDataServiceImpl extends BaseServiceEsImpl<KidLessonData, String> implements KidLessonDataService{

    public KidLessonDataServiceImpl(ElasticSearchTransportClient transport, SpecificationsBuilder specificationsBuilder) {
        super(transport, specificationsBuilder);
    }

    @Override
    protected String getIndex() {
        return "kid_lesson";
    }

    @Override
    protected Class<KidLessonData> getClassType() {
        return KidLessonData.class;
    }

    @Override
    public Page<KidLessonData> find(KidLessonFilterDTO filterDTO) {
//        log.info("(find) - with request: [{}]", filterDTO);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termQuery("id", 96));

        Pageable pageable = PageRequest.of(0, 10);

        return search(boolQueryBuilder, buildSort(pageable), pageable);
    }
}
