package site.foxcode.springbootdemo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import site.foxcode.common.service.repository.CustomRepository;
import site.foxcode.common.service.service.impl.BaseServiceSqlImpl;
import site.foxcode.springbootdemo.entity.UserEntity;
import site.foxcode.springbootdemo.repository.UserRepository;

@Service(value = "userService")
@RequiredArgsConstructor
public class UserServiceImpl extends BaseServiceSqlImpl<UserEntity, String> implements UserService {
    private final UserRepository userRepository;
    @Override
    public CustomRepository<UserEntity, String> getRepository() {
        return userRepository;
    }
}
