package site.foxcode.springbootdemo.service.es;

import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import site.foxcode.common.service.service.BaseService;
import site.foxcode.springbootdemo.dto.KidLessonFilterDTO;
import site.foxcode.springbootdemo.entity.KidLessonData;


public interface KidLessonDataService extends BaseService<KidLessonData, String> {

    Page<KidLessonData> find(KidLessonFilterDTO filterDTO);

    default  SortBuilder buildSort(Pageable pageable) {
        SortBuilder sortBuilder = null;
        for (Sort.Order order : pageable.getSort()) {
//            log.debug("(buildSort)Property: " + order.getProperty());
//            log.debug("(buildSort)Direction: " + order.getDirection());
            SortOrder sortOrder = SortOrder.ASC;
            if (order.getDirection().isDescending()) {
                sortOrder = SortOrder.DESC;
            }
//            log.debug("(buildSort)sortOrder: " + order);
            sortBuilder = SortBuilders.fieldSort(order.getProperty()).order(sortOrder);
        }

        return sortBuilder;
    }
}
