package site.foxcode.springbootdemo.repository;

import site.foxcode.common.service.repository.CustomRepository;
import site.foxcode.springbootdemo.entity.UserEntity;

import java.util.List;

public interface UserRepository extends CustomRepository<UserEntity, String> {

}
