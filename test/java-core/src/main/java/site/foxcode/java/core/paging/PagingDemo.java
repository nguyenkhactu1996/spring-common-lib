package site.foxcode.java.core.paging;

import java.util.Scanner;

public class PagingDemo {
    private final int totalPage;
    private final int pagingBarSize;
    private final int currentPage;

    public PagingDemo(int totalPage, int pagingBarSize, int currentPage) {
        this.totalPage = totalPage;
        this.pagingBarSize = pagingBarSize;
        this.currentPage = currentPage;
    }

    public void genPage(int page) {
        System.out.println(page);
    }

    public void paging() {
        int middle = pagingBarSize / 2;
        int startOfPaging = pagingBarSize % 2 == 0 ? currentPage - middle + 1 : currentPage - middle;
        int endOfPaging = currentPage + middle;
        if (startOfPaging >= 1 && endOfPaging <= totalPage) {
            for (int page = startOfPaging; page <= endOfPaging; page++) {
                genPage(page);
            }
            return;
        }
        if (startOfPaging < 1) {
            if (pagingBarSize <= totalPage) {
                for (int page = 1; page <= pagingBarSize; page++) {
                    genPage(page);
                }
            } else {
                for (int page = 1; page <= totalPage; page++) {
                    genPage(page);
                }
            }
            return;
        }

        if (endOfPaging >= totalPage) {
            if (pagingBarSize <= totalPage) {
                for (int offset = pagingBarSize - 1; offset >= 0; offset--) {
                    genPage(totalPage - offset);
                }
            } else {
                for (int offset = totalPage - 1; offset >= 0; offset--) {
                    genPage(totalPage - offset);
                }
            }
            return;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Nhập vào trang hiện tại:");
            int currentPage = scanner.nextInt();
            PagingDemo pagingDemo = new PagingDemo(13, 5, currentPage);
            pagingDemo.paging();
        }
    }
}
