package site.foxcode.springbootsecurityjwt.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import site.foxcode.ezlearning.core.entity.LessonEntity;
import site.foxcode.ezlearning.core.repository.LessonRepository;
import site.foxcode.springbootsecurityjwt.service.LessonService;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;
    @Override
    public Page<LessonEntity> findAllPaging(Pageable pageable) {
        return lessonRepository.findAll(pageable);
    }

    @Override
    public Optional<LessonEntity> findById(Integer id) {
        return lessonRepository.findById(id);
    }
}
