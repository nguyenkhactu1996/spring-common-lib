package site.foxcode.springbootsecurityjwt.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import site.foxcode.ezlearning.core.entity.LessonEntity;

import java.util.Optional;

public interface LessonService {
    Page<LessonEntity> findAllPaging(Pageable pageable);

    Optional<LessonEntity> findById(Integer id);
}
