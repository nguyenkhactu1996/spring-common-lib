package site.foxcode.springbootsecurityjwt.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import site.foxcode.ezlearning.core.entity.LessonEntity;
import site.foxcode.springbootsecurityjwt.service.LessonService;

@CrossOrigin(origins = "*", maxAge = 3600)
@Slf4j
@RestController
@RequestMapping(value = "/api/lessons")
@RequiredArgsConstructor
public class LessonController {
    private final LessonService lessonService;

    @GetMapping
    public ResponseEntity<Page<LessonEntity>> getAll(@RequestParam Integer page, @RequestParam Integer size){
        log.info("Get all lesson with page: [{}] and size: [{}]", page, size);
        return ResponseEntity.ok(lessonService.findAllPaging(PageRequest.of(page, size)));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LessonEntity> findById(@PathVariable Integer id){
        return lessonService.findById(id).map(lessonEntity -> ResponseEntity.ok(lessonEntity))
                .orElse(ResponseEntity.notFound().build());
    }
}
