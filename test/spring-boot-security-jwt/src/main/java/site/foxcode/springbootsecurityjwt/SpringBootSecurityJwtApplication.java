package site.foxcode.springbootsecurityjwt;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import site.foxcode.common.service.repository.CustomRepositoryImpl;
import site.foxcode.ezlearning.core.entity.LessonEntity;
import site.foxcode.ezlearning.core.repository.LessonRepository;
import site.foxcode.springbootsecurityjwt.config.ApplicationConfig;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class SpringBootSecurityJwtApplication implements CommandLineRunner {
    private final ApplicationConfig config;
    private final LessonRepository lessonRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityJwtApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Command is running ....");
        log.info("Jwt secret: [{}]", config.getJwtConfig().getJwtSecret());
        lessonRepository.findById(90001).ifPresent(lesson -> {
            log.info(lesson.getName());
        });
    }
}
