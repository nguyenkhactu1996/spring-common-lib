package site.foxcode.springbootsecurityjwt.config;

import org.springframework.context.annotation.Configuration;
import site.foxcode.ezlearning.core.config.EnableEzlearningCore;

@Configuration
@EnableEzlearningCore
public class ExternalModuleConfiguration {
}
