package site.foxcode.springbootsecurityjwt.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationConfig {

    private JwtConfig jwtConfig;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JwtConfig {
        private String jwtSecret;
        private Long jwtExpirationMs;
    }
}
