package site.foxcode.readcsvfiletest;

import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ComponentTest {
    RateLimiter rateLimiter = RateLimiter.create(1);

    public void doSomeThing() {
        rateLimiter.acquire();
        log.info("ComponentTest doSomeThing");
    }
}
