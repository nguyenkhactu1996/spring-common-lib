package site.foxcode.readcsvfiletest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class ReadCsvFileTestApplication implements CommandLineRunner {
    private final ComponentTest componentTest;

    public static void main(String[] args) {
        SpringApplication.run(ReadCsvFileTestApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Command is running ...");
        while (true){
            componentTest.doSomeThing();
        }
    }
}
