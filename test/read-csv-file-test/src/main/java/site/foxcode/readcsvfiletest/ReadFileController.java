package site.foxcode.readcsvfiletest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/test/file")
@Slf4j
public class ReadFileController {
    @GetMapping
    public ResponseEntity readFile(@RequestParam("url") String url) {
        String fullPath = url + "?code=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJDQ00iLCJpYXQiOjE2NzY0NDgwOTgsImV4cCI6MjU5MTgwODA5OH0.M5k4_cWexjSJtWc_A5wUKz0ppsSHq25zEeOEkXuUoCqFpx1nk63C4yAZj91QJJEMdM-00MKo1WHHgARB1Jj5XA";
        log.info("(readFile) - begin read file: {}", fullPath);
        List<List<String>> result = readCSV(fullPath);
        log.info("(readFile) - end read file: {}", result.size());
        return ResponseEntity.noContent().build();
    }

    public List<List<String>> readCSV(String filePath) {
        List<List<String>> results = new ArrayList<>();
        try {
            InputStream inputStream = new URL(filePath).openStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream,
                    StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] split = line.split("\\,");
                results.add(Arrays.asList(split));
            }
            reader.close();
        } catch (Exception ex) {
            log.info("(read) ERROR, filePath: {}", ex);
            ex.printStackTrace();
        }
        return results;
    }
}
