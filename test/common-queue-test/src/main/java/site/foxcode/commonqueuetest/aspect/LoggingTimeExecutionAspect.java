package site.foxcode.commonqueuetest.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class LoggingTimeExecutionAspect {

    @Around("execution(* site.foxcode.commonqueuetest.service.NotificationService.notify(..))")
    public void logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("(logExecutionTime) - Calculating .... [{}]",
                joinPoint.getSignature().getName());

        joinPoint.proceed();

        log.info("(logExecutionTime) - Calculated done!");
    }
}
