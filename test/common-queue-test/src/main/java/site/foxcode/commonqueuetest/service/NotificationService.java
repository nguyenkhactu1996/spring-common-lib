package site.foxcode.commonqueuetest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotificationService {

    public void notify(String target, String message){
        //Do something to push notification
        log.info("(notify) - push notification to : [{}] with message: [{}]",
                target,
                message);
    }
}
