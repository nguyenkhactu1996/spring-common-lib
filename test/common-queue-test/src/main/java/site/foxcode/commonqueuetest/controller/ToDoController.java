package site.foxcode.commonqueuetest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.foxcode.commonqueuetest.entity.ToDo;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/todo")
public class ToDoController {

    @GetMapping
    public ResponseEntity<List<ToDo>> getList(){
        List<ToDo> toDoList = new ArrayList<>();
        for(int i = 0; i< 2; i++){
            toDoList.add(new ToDo(i, "Hoàn thành công việc " + i));
        }
        return ResponseEntity.ok(toDoList);
    }
}
