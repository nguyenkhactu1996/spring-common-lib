package site.foxcode.commonqueuetest.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import site.foxcode.commonqueuetest.entity.UserDTO;

@Component
@Slf4j
public class TestListener {
    @RabbitListener(queues = "test.common.queue")
    public void testListener(UserDTO msg){
        log.info("Msg: [{}]", msg);
    }
}
