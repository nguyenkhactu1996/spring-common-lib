package site.foxcode.commonqueuetest.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {
    @Bean
    public Queue testCommonQueue(){
        return new Queue("test.common.queue", true);
    }
}
