package site.foxcode.logbacklogstashdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import site.foxcode.logbacklogstashdemo.entity.User;

import java.util.UUID;

@SpringBootApplication
@Slf4j
public class LogbackLogstashDemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(LogbackLogstashDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        long count = 0;
        while (true){
            Thread.sleep(1000);
            count++;
            log.info("Person with count: [{}] person: [{}]", count, new User("name" + count, UUID.randomUUID().toString()));
        }
    }
}
