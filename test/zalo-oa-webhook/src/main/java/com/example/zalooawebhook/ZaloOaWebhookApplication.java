package com.example.zalooawebhook;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import site.foxcode.common.queue.service.QueueService;

@SpringBootApplication
@Slf4j
public class ZaloOaWebhookApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ZaloOaWebhookApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Command is running ....");
    }
}
