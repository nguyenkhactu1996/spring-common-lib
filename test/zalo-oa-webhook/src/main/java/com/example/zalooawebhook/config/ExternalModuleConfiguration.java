package com.example.zalooawebhook.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import site.foxcode.common.queue.config.EnableCommonQueueConsumer;
import site.foxcode.common.queue.config.EnableCommonQueuePublisher;

@Configuration
@EnableCommonQueuePublisher
@EnableCommonQueueConsumer
public class ExternalModuleConfiguration {
    @Bean
    public Queue testCommonQueue(){
        return new Queue("ccms.oa.webhook.data", true);
    }
}
