package com.example.zalooawebhook.listener;

import com.example.zalooawebhook.TestRateLimit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class OAListener {

//    @Value("${ccm_webhook_endpoint:http://172.25.80.25:30124/hook/zalo}")
//    private String ccmWebhookEndpoint;

    private final TestRateLimit testRateLimit;

    @RabbitListener(queues = "ccms.oa.webhook.data")
    public void listen(String msg) throws JsonProcessingException {
//        sendMessageToDevWebhook(msg);
//        log.info("{}", msg);
        testRateLimit.test(msg);
    }

    public void sendMessageToDevWebhook(String msg) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Map<Object, Object> body = new ObjectMapper().readValue(msg, Map.class);


        HttpEntity<Map> requestEntity =
                new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

//        restTemplate.postForLocation("http://192.168.1.236:6970/hook/zalo", requestEntity);
//        restTemplate.postForLocation("http://localhost:8080/hook/zalo",requestEntity);
    }
}
