package com.example.zalooawebhook;

import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TestRateLimit {
    RateLimiter rateLimiter = RateLimiter.create(1);
    public void test(String data){
        rateLimiter.acquire();
        log.info("Data: [{}]", data);
    }
}
