package com.example.zalooawebhook;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.foxcode.common.queue.service.QueueService;

@RestController
@RequestMapping(value = "/zalo/oa")
@Slf4j
@RequiredArgsConstructor
public class ZaloOAWebhookController {
    private final QueueService queueService;

    private final static String TOPIC = "ccms.oa.webhook.data";

    @PostMapping(value = "/webhook")
    public ResponseEntity hook(@RequestBody String data){
        log.info("{}", data);
        queueService.push(data, TOPIC);
        return ResponseEntity.ok().build();
    }
}
