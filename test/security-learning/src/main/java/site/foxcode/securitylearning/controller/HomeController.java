package site.foxcode.securitylearning.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/home")
public class HomeController {

    @GetMapping
    public String home(Principal principal){
        return "Home: " + principal.getName();
    }
}
