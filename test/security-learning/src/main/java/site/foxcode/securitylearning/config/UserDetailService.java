package site.foxcode.securitylearning.config;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDetailService implements UserDetailsService {
    public static List<UserDetails> userDetails = new ArrayList<>();

    static {
        userDetails.add(User.withUsername("tunk")
                        .password("tunk")
                        .disabled(false)
                        .accountExpired(false)
                        .accountLocked(false)
                        .authorities(new String[]{"user"})
                .build());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDetails.stream().filter(detail -> detail.getUsername().equals(username)).collect(Collectors.toList()).get(0);
    }
}
