package site.foxcode.foxconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class FoxConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoxConfigServerApplication.class, args);
    }

}
