--liquibase formatted sql
--changeset foxcodesite:create-table-license dbms:mysql
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT count(*) FROM information_schema.tables where table_name = 'license' and TABLE_SCHEMA = (SELECT DATABASE());
create table `license` (
                         `license_id` varchar(50) primary key not null,
                         `description` text,
                         `organization_id` varchar (50) not null,
                         `product_name` varchar(255) not null,
                         `license_type` varchar(50),
                         `comment` text
);

--rollback drop table `license`;