package site.foxcode.licensingservice.service;

import site.foxcode.licensingservice.entity.License;

public interface LicenseService {
    License getLicense(String licenseId, String organizationId);
    License createLicense(License license);
    License updateLicense(License license);
    String deleteLicense(String licenseId);
}
