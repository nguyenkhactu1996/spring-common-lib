package site.foxcode.licensingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import site.foxcode.licensingservice.entity.License;

import java.util.List;

public interface LicenseRepository extends JpaRepository<License, String> {
    List<License> findByOrganizationId(String organizationId);
    License findByOrganizationIdAndLicenseId(String organizationId, String licenseId);
}
