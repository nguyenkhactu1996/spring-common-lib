package site.foxcode.common.queue.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.SerializerMessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import site.foxcode.common.queue.constant.ConverterType;
import site.foxcode.common.queue.service.QueueService;
import site.foxcode.common.queue.service.QueueServiceRabbitMqImpl;

import javax.annotation.PostConstruct;

public class RabbitmqPublisherConfiguration {
    private static final Logger log = LoggerFactory.getLogger(RabbitmqPublisherConfiguration.class);

    @Value("${application.common-queue.rabbitmq.default-converter:Jackson2Json}")
    private ConverterType converterType;

    public RabbitmqPublisherConfiguration() {
    }

    @PostConstruct
    public void postConstruct() {
        log.info("(init)RabbitmqConfiguration");
    }

    @Bean
    public QueueService queueServiceRabbitMQ(RabbitTemplate template, RabbitTemplate templateSimple) {
        return new QueueServiceRabbitMqImpl(template, templateSimple);
    }

    @Bean(
            name = {"rabbitTemplate"}
    )
    @Primary
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        if (this.converterType.equals(ConverterType.Jackson2Json)) {
            template.setMessageConverter(new Jackson2JsonMessageConverter());
        }

        if (this.converterType.equals(ConverterType.Serializer)) {
            template.setMessageConverter(new SerializerMessageConverter());
        }

        return template;
    }

    @Bean(
            name = {"rabbitTemplateSimple"}
    )
    public RabbitTemplate rabbitTemplateSimple(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(new SimpleMessageConverter());
        return template;
    }
}
