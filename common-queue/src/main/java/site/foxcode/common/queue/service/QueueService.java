package site.foxcode.common.queue.service;

import java.util.List;

public interface QueueService {
    void push(String var1, String var2);

    void push(Object var1, String var2);

    void push(List<Object> var1, String var2);
}
