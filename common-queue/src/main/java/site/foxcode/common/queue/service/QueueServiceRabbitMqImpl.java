package site.foxcode.common.queue.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Iterator;
import java.util.List;

public class QueueServiceRabbitMqImpl implements QueueService{
    private static final Logger log = LoggerFactory.getLogger(QueueServiceRabbitMqImpl.class);
    private final RabbitTemplate template;
    private final RabbitTemplate templateSimple;

    public QueueServiceRabbitMqImpl(RabbitTemplate template, RabbitTemplate templateSimple) {
        this.template = template;
        this.templateSimple = templateSimple;
    }

    public void push(String message, String queue) {
        log.info("(push)message: {}, queue: {}", message, queue);
        this.templateSimple.convertAndSend(queue, message);
    }

    public void push(Object message, String queue) {
        log.info("(push)message: {}, queue: {}", message, queue);
        this.template.convertAndSend(queue, message);
    }

    public void push(List<Object> messages, String queue) {
        log.info("(push)queue: {}, messages: {}", queue, messages.size());
        Iterator var3 = messages.iterator();

        while(var3.hasNext()) {
            Object message = var3.next();
            this.template.convertAndSend(queue, message);
        }

    }
}
