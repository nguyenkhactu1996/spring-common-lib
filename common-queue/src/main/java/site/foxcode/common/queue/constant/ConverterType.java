package site.foxcode.common.queue.constant;

public enum ConverterType {
    Jackson2Json,
    Serializer;

    private ConverterType() {
    }
}
