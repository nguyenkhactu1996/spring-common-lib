package site.foxcode.common.queue.config;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.SerializerMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import site.foxcode.common.queue.constant.ConverterType;

public class RabbitmqConsumerConfiguration {
    @Value("${application.common-queue.rabbitmq.default-converter:Jackson2Json}")
    private ConverterType converterType;
    @Value("${application.common-queue.rabbitmq.concurrent.consumers:5}")
    private int concurrentConsumers;
    @Value("${application.common-queue.rabbitmq.max.concurrent.consumers:30}")
    private int maxConcurrentConsumers;
    @Value("${application.common-queue.rabbitmq.prefetch:1}")
    private int prefetch;

    public RabbitmqConsumerConfiguration() {
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConcurrentConsumers(this.concurrentConsumers);
        factory.setMaxConcurrentConsumers(this.maxConcurrentConsumers);
        factory.setPrefetchCount(this.prefetch);
        factory.setConnectionFactory(connectionFactory);
        if (this.converterType.equals(ConverterType.Jackson2Json)) {
            factory.setMessageConverter(new Jackson2JsonMessageConverter());
        }

        if (this.converterType.equals(ConverterType.Serializer)) {
            factory.setMessageConverter(new SerializerMessageConverter());
        }

        return factory;
    }
}
