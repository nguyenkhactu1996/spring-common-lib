package site.foxcode.jpa.common;

public enum FieldType {
    STRING,
    BOOLEAN,
    DATE,
    INTEGER,
    DOUBLE,
    FLOAT
}
