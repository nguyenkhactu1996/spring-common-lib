package site.foxcode.jpa.common;

public enum SearchOperation {
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    LESS_THAN,
    GREATER_OR_EQUAL,
    LESS_OR_EQUAL,
    IN,
    NOT_IN,
    LIKE
}
