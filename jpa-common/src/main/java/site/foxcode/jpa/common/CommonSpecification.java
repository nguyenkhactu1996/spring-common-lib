package site.foxcode.jpa.common;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CommonSpecification<T> implements Specification<T> {
    private List<SearchCriteria> andList = new LinkedList<>();
    private List<SearchCriteria> orList = new LinkedList<>();
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> andList = processAndList(root, query, criteriaBuilder);
        List<Predicate> orList = processOrList(root, query, criteriaBuilder);
        Predicate and = andList.size() == 0 ? criteriaBuilder.isTrue(criteriaBuilder.literal(true)): criteriaBuilder.and(andList.toArray(new Predicate[0]));
        Predicate or = orList.size() == 0 ? criteriaBuilder.isTrue(criteriaBuilder.literal(true)) :  criteriaBuilder.or(orList.toArray(new Predicate[0]));
        return criteriaBuilder.and(Arrays.asList(and, or).toArray(new Predicate[0]));
    }

    private List<Predicate> processAndList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder){
        return processCriteriaList(root, query, criteriaBuilder, andList);
    }

    private List<Predicate> processOrList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder){
        return processCriteriaList(root, query, criteriaBuilder, orList);
    }

    private List<Predicate> processCriteriaList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, List<SearchCriteria> criteriaList){
        List<Predicate> predicates = new LinkedList<>();
        for (SearchCriteria searchCriteria : criteriaList) {
            Predicate predicate = processSearchCriteria(root, query, criteriaBuilder, searchCriteria);
            if(predicate != null){
                predicates.add(predicate);
            }
        }
        return predicates;
    }


    private Predicate processSearchCriteria(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, SearchCriteria criteria){
        Predicate predicate = null;
        switch (criteria.getSearchOperation()){
            case EQUAL:
                predicate = criteriaBuilder.equal(root.get(criteria.getField()), criteria.getValue());
            case IN:
                predicate = root.get(criteria.getField()).in(criteria.getValue());
            case LIKE:
                break;
            case GREATER_OR_EQUAL:
                break;
            case NOT_IN:
                break;
            case LESS_THAN:
                break;
            case NOT_EQUAL:
                break;
            case GREATER_THAN:
                break;
            case LESS_OR_EQUAL:
                break;
        }
        return predicate;
    }


    public CommonSpecification<T> withAnd(SearchCriteria searchCriteria){
        this.andList.add(searchCriteria);
        return this;
    }

    public CommonSpecification<T> withOr(SearchCriteria searchCriteria){
        this.orList.add(searchCriteria);
        return this;
    }

    public static CommonSpecification newInstance(){
        return new CommonSpecification();
    }
}
