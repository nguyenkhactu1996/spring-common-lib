package site.foxcode.jpa.common;

import javax.persistence.metamodel.SingularAttribute;

public class SearchCriteria <X, Y> {

    private SingularAttribute<X, Y> field;

    private SearchOperation searchOperation;

    private FieldType fieldType;

    private Object value;


    public SearchOperation getSearchOperation() {
        return searchOperation;
    }

    public SingularAttribute<X, Y> getField() {
        return field;
    }

    public void setField(SingularAttribute<X, Y> field) {
        this.field = field;
    }

    public void setSearchOperation(SearchOperation searchOperation) {
        this.searchOperation = searchOperation;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
