package site.foxcode.onlinestoreweb.service;

import org.springframework.stereotype.Service;
import site.foxcode.online_store.core.entity.CategoryEntity;
import site.foxcode.online_store.core.repository.CategoryRepository;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService{
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Optional<CategoryEntity> findById(String id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void save(CategoryEntity categoryEntity) {
        categoryRepository.save(categoryEntity);
    }
}
