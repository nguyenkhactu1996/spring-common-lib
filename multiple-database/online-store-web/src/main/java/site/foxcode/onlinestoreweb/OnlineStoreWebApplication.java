package site.foxcode.onlinestoreweb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import site.foxcode.online_store.core.entity.CategoryEntity;
import site.foxcode.onlinestoreweb.service.CategoryService;

import java.time.LocalDateTime;
import java.util.UUID;

@SpringBootApplication
public class OnlineStoreWebApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(OnlineStoreWebApplication.class);

    private final CategoryService categoryService;

    public OnlineStoreWebApplication(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public static void main(String[] args) {
        SpringApplication.run(OnlineStoreWebApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Command is running ...");
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(UUID.randomUUID().toString());
        categoryEntity.setName("TunkTest");
        categoryEntity.setSlug("TunkTest");
        categoryEntity.setCreatedAt(LocalDateTime.now());
        categoryEntity.setCreatedBy("SYSTEM");
        categoryEntity.setUpdatedAt(LocalDateTime.now());
        categoryEntity.setUpdatedBy("SYSTEM");

        categoryService.save(categoryEntity);

        log.info("Save done!");
    }
}
