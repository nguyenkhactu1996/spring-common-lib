package site.foxcode.onlinestoreweb.config;

import org.springframework.context.annotation.Configuration;
import site.foxcode.online_store.core.config.EnableOnlineStoreCore;

@Configuration
@EnableOnlineStoreCore
public class ExternalModuleConfiguration {

}
