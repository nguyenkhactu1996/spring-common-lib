package site.foxcode.onlinestoreweb.service;

import site.foxcode.online_store.core.entity.CategoryEntity;

import java.util.Optional;

public interface CategoryService {
    Optional<CategoryEntity> findById(String id);

    void save(CategoryEntity categoryEntity);
}
