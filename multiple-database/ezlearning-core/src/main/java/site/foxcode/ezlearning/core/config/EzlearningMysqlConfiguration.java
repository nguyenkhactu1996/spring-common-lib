package site.foxcode.ezlearning.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import site.foxcode.common.service.repository.CustomRepositoryImpl;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(
        basePackages = "site.foxcode.ezlearning.core.repository",
        entityManagerFactoryRef = "ezlearningEntityManager",
        repositoryBaseClass = CustomRepositoryImpl.class,
        transactionManagerRef = "ezlearningTransactionManager"
)
public class EzlearningMysqlConfiguration {
    private static final Logger log = LoggerFactory.getLogger(EzlearningMysqlConfiguration.class);

    private final Environment env;


    public EzlearningMysqlConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean ezlearningEntityManager() {
        log.info("(ezlearningEntityManager)dialect: {}", env.getProperty("spring.mysql-connection.hibernate.dialect"));
        log.info("(ezlearningEntityManager)url: {}", env.getProperty("spring.mysql-connection.ezlearning.jdbcUrl"));
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        DataSource dataSource = onlineStoreDataSource();
        em.setDataSource(onlineStoreDataSource());
        em.setPackagesToScan("site.foxcode.ezlearning.core.entity");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put(
                "hibernate.dialect",
                env.getProperty("spring.mysql-connection.hibernate.dialect")
        );
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.mysql-connection.ezlearning")
    public DataSource onlineStoreDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public PlatformTransactionManager ezlearningTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(ezlearningEntityManager().getObject());
        return transactionManager;
    }
}
