package site.foxcode.ezlearning.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lessons")
public class LessonEntity {
    @Id
    private Integer id;

    private String name;

    @Column(name = "is_deleted")
    private Short isDeleted;

    @Column(name = "subject_id")
    private Integer subjectId;

    @Column(name = "parent_id")
    private Integer parentId;

    public LessonEntity(Integer id, String name, Short isDeleted, Integer subjectId, Integer parentId) {
        this.id = id;
        this.name = name;
        this.isDeleted = isDeleted;
        this.subjectId = subjectId;
        this.parentId = parentId;
    }

    public LessonEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Short isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
