package site.foxcode.ezlearning.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import site.foxcode.ezlearning.core.entity.LessonEntity;

public interface LessonRepository extends JpaRepository<LessonEntity, Integer> {
}
