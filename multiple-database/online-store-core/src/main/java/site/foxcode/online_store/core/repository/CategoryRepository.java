package site.foxcode.online_store.core.repository;

import site.foxcode.online_store.core.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity, String> {
}
