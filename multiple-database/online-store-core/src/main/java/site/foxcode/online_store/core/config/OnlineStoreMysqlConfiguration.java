package site.foxcode.online_store.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import site.foxcode.common.service.repository.CustomRepositoryImpl;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(
        basePackages = "site.foxcode.online_store.core.repository",
        entityManagerFactoryRef = "oldCrmEntityManager",
        repositoryBaseClass = CustomRepositoryImpl.class,
        transactionManagerRef = "onlineStoreTransactionManager"
)
public class OnlineStoreMysqlConfiguration {
    private static final Logger log = LoggerFactory.getLogger(OnlineStoreMysqlConfiguration.class);

    private final Environment env;


    public OnlineStoreMysqlConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean oldCrmEntityManager() {
        log.info("(onlineStoreEntityManager)dialect: {}", env.getProperty("spring.mysql-connection.hibernate.dialect"));
        log.info("(onlineStoreEntityManager)url: {}", env.getProperty("spring.mysql-connection.online-store.jdbcUrl"));
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(onlineStoreDataSource());
        em.setPackagesToScan("site.foxcode.online_store.core.entity");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put(
                "hibernate.dialect",
                env.getProperty("spring.mysql-connection.hibernate.dialect")
        );
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.mysql-connection.online-store")
    public DataSource onlineStoreDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public PlatformTransactionManager onlineStoreTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(oldCrmEntityManager().getObject());
        return transactionManager;
    }
}
