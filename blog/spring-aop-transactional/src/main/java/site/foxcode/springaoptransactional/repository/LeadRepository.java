package site.foxcode.springaoptransactional.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import site.foxcode.springaoptransactional.entity.LeadEntity;

public interface LeadRepository extends JpaRepository<LeadEntity, Long> {

}
