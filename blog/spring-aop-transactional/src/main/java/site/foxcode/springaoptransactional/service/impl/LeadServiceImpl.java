package site.foxcode.springaoptransactional.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.foxcode.springaoptransactional.entity.LeadEntity;
import site.foxcode.springaoptransactional.repository.LeadRepository;
import site.foxcode.springaoptransactional.service.LeadService;

@Service
@RequiredArgsConstructor
@Slf4j
public class LeadServiceImpl implements LeadService {
    private final LeadRepository leadRepository;

    @Override
    @Async
    @Transactional
    public void createLead(LeadEntity lead) {
        log.info("Create lead");
        leadRepository.save(lead);
        throw new RuntimeException("Oops! Something went wrong!");
    }

    @Override
    public void createLeadInternal(LeadEntity lead) {
        createLead(lead);
    }
}
