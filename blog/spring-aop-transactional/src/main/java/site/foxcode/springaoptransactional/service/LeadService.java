package site.foxcode.springaoptransactional.service;

import site.foxcode.springaoptransactional.entity.LeadEntity;

public interface LeadService {
    void createLead(LeadEntity lead);

    void createLeadInternal(LeadEntity lead);
}
