package site.foxcode.springaoptransactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import site.foxcode.springaoptransactional.entity.LeadEntity;
import site.foxcode.springaoptransactional.entity.LeadStatus;
import site.foxcode.springaoptransactional.service.LeadService;

@SpringBootApplication
//@RequiredArgsConstructor
@EnableAsync
@Slf4j
public class SpringAopTransactionalApplication implements CommandLineRunner {
    private LeadService leadService;
    public static void main(String[] args) {
        SpringApplication.run(SpringAopTransactionalApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LeadEntity leadEntity = new LeadEntity();
        leadEntity.setCustomerName("tunk");
        leadEntity.setPhone("0963256124");
        leadEntity.setLeadStatus(LeadStatus.NEW);
        leadService.createLeadInternal(leadEntity);
    }

    @Autowired
    public void setLeadService(LeadService leadService) {
        log.info("Setter is calling ...");
        this.leadService = leadService;
    }
}
