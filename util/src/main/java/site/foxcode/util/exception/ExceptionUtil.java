package site.foxcode.util.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtil {
    public ExceptionUtil() {
    }

    public static String getFullStackTrace(Exception e) {
        return "(getFullStackTrace)" + getFullStackTraceRaw(e);
    }

    public static String getFullStackTrace(Exception e, Boolean isRaw) {
        return isRaw ? getFullStackTraceRaw(e) : "(getFullStackTrace)" + getFullStackTraceRaw(e).replaceAll("\n", "[NEW_LINE]");
    }

    public static String getFullStackTraceRaw(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();
        return stackTrace;
    }
}
