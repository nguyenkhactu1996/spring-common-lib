package site.foxcode.elasticsearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ElasticsearchProperties.class})
public class ElasticsearchConfiguration {
    private static final Logger log = LoggerFactory.getLogger(ElasticsearchConfiguration.class);

    private final ElasticsearchProperties elasticsearchProperties;


    public ElasticsearchConfiguration(ElasticsearchProperties elasticsearchProperties) {
        this.elasticsearchProperties = elasticsearchProperties;
    }

    @Bean
    public ElasticSearchTransportClient elasticSearchTransportClient() {
        return new ElasticSearchTransportClient(this.elasticsearchProperties);
    }
}
