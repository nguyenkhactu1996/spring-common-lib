package site.foxcode.elasticsearch;

public class SearchCriteria {
    private String key;
    private SearchOperation operation;
    private Object value;

    private SearchCriteria(String key, SearchOperation operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public static SearchCriteria of(String key, SearchOperation operation, Object value) {
        return new SearchCriteria(key, operation, value);
    }

    public String getKey() {
        return this.key;
    }

    public SearchOperation getOperation() {
        return this.operation;
    }

    public Object getValue() {
        return this.value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setOperation(SearchOperation operation) {
        this.operation = operation;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof SearchCriteria)) {
            return false;
        } else {
            SearchCriteria other = (SearchCriteria)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$key = this.getKey();
                    Object other$key = other.getKey();
                    if (this$key == null) {
                        if (other$key == null) {
                            break label47;
                        }
                    } else if (this$key.equals(other$key)) {
                        break label47;
                    }

                    return false;
                }

                Object this$operation = this.getOperation();
                Object other$operation = other.getOperation();
                if (this$operation == null) {
                    if (other$operation != null) {
                        return false;
                    }
                } else if (!this$operation.equals(other$operation)) {
                    return false;
                }

                Object this$value = this.getValue();
                Object other$value = other.getValue();
                if (this$value == null) {
                    if (other$value != null) {
                        return false;
                    }
                } else if (!this$value.equals(other$value)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof SearchCriteria;
    }

//    public int hashCode() {
//        int PRIME = true;
//        int result = 1;
//        Object $key = this.getKey();
//        int result = result * 59 + ($key == null ? 43 : $key.hashCode());
//        Object $operation = this.getOperation();
//        result = result * 59 + ($operation == null ? 43 : $operation.hashCode());
//        Object $value = this.getValue();
//        result = result * 59 + ($value == null ? 43 : $value.hashCode());
//        return result;
//    }

    public String toString() {
        return "SearchCriteria(key=" + this.getKey() + ", operation=" + this.getOperation() + ", value=" + this.getValue() + ")";
    }
}
