package site.foxcode.elasticsearch;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(
        prefix = "elastic.search"
)
public class ElasticsearchProperties {
    private boolean clusterEnable;
    private String clusterName;
    private String host;
    private List<String> hosts;
    private int port;

    public ElasticsearchProperties() {
    }

    public boolean isClusterEnable() {
        return this.clusterEnable;
    }

    public String getClusterName() {
        return this.clusterName;
    }

    public String getHost() {
        return this.host;
    }

    public List<String> getHosts() {
        return this.hosts;
    }

    public int getPort() {
        return this.port;
    }

    public void setClusterEnable(boolean clusterEnable) {
        this.clusterEnable = clusterEnable;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setHosts(List<String> hosts) {
        this.hosts = hosts;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ElasticsearchProperties)) {
            return false;
        } else {
            ElasticsearchProperties other = (ElasticsearchProperties)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.isClusterEnable() != other.isClusterEnable()) {
                return false;
            } else if (this.getPort() != other.getPort()) {
                return false;
            } else {
                label52: {
                    Object this$clusterName = this.getClusterName();
                    Object other$clusterName = other.getClusterName();
                    if (this$clusterName == null) {
                        if (other$clusterName == null) {
                            break label52;
                        }
                    } else if (this$clusterName.equals(other$clusterName)) {
                        break label52;
                    }

                    return false;
                }

                Object this$host = this.getHost();
                Object other$host = other.getHost();
                if (this$host == null) {
                    if (other$host != null) {
                        return false;
                    }
                } else if (!this$host.equals(other$host)) {
                    return false;
                }

                Object this$hosts = this.getHosts();
                Object other$hosts = other.getHosts();
                if (this$hosts == null) {
                    if (other$hosts != null) {
                        return false;
                    }
                } else if (!this$hosts.equals(other$hosts)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof ElasticsearchProperties;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + (this.isClusterEnable() ? 79 : 97);
        result = result * 59 + this.getPort();
        Object $clusterName = this.getClusterName();
        result = result * 59 + ($clusterName == null ? 43 : $clusterName.hashCode());
        Object $host = this.getHost();
        result = result * 59 + ($host == null ? 43 : $host.hashCode());
        Object $hosts = this.getHosts();
        result = result * 59 + ($hosts == null ? 43 : $hosts.hashCode());
        return result;
    }

    public String toString() {
        return "ElasticsearchProperties(clusterEnable=" + this.isClusterEnable() + ", clusterName=" + this.getClusterName() + ", host=" + this.getHost() + ", hosts=" + this.getHosts() + ", port=" + this.getPort() + ")";
    }
}
