package site.foxcode.elasticsearch;

import java.util.Arrays;

public enum SearchOperation {
    EQUALITY("=="),
    NEGATION("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    LIKE("~");

    private String operator;

    private SearchOperation(String operator) {
        this.operator = operator;
    }

    public static SearchOperation getSearchOperation(String param) {
        return (SearchOperation) Arrays.stream(values()).filter((operation) -> {
            return param.contains(operation.getOperator());
        }).findAny().orElse((SearchOperation) null);
    }

    public String getOperator() {
        return this.operator;
    }
}
