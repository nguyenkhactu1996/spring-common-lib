package site.foxcode.elasticsearch;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

@Configuration
@Import({ElasticsearchConfiguration.class})
public class EsConfiguration {
    public EsConfiguration() {
    }

    @Bean
    @Scope("prototype")
    public SpecificationsBuilder specificationsBuilder() {
        return new SpecificationsBuilder();
    }
}
