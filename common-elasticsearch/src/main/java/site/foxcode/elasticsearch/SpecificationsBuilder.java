package site.foxcode.elasticsearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SpecificationsBuilder {
    private static final Logger log = LoggerFactory.getLogger(SpecificationsBuilder.class);

    public SpecificationsBuilder() {
    }

    public List<SearchCriteria> build(String search) {
        log.info("(build)search: {}", search);
        if (search != null && !search.isEmpty()) {
            List<SearchCriteria> criteriaList = new ArrayList();
            String[] params = search.split(";");
            String[] var4 = params;
            int var5 = params.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String param = var4[var6];
                criteriaList.add(this.getSearchCriteria(param));
            }

            log.info("(build)criteriaList: {}", criteriaList);
            return criteriaList;
        } else {
            return new ArrayList();
        }
    }

    private SearchCriteria getSearchCriteria(String param) {
        log.info("(getSearchCriteria)param: {}", param);
        SearchOperation operation = SearchOperation.getSearchOperation(param);
        if (operation == null) {
            return null;
        } else {
            String[] parts = param.split(operation.getOperator());
            String key = parts[0];
            if (key.equals("id")) {
                key = "_id";
            }

            return SearchCriteria.of(key, operation, parts[1]);
        }
    }
}
