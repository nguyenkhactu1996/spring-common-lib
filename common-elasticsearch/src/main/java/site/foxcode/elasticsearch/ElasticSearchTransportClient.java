package site.foxcode.elasticsearch;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ElasticSearchTransportClient {
    private static final Logger log = LoggerFactory.getLogger(ElasticSearchTransportClient.class);

    private final ElasticsearchProperties elasticsearchProperties;
    private RestHighLevelClient client = null;

    public ElasticSearchTransportClient(ElasticsearchProperties elasticsearchProperties) {
        this.elasticsearchProperties = elasticsearchProperties;
    }

    public RestHighLevelClient getClient() {
        log.info("(getClient)elasticsearchConfig: {}", elasticsearchProperties);
//        log.info("(getClient)elasticsearchConfig: {}", MapperUtil.toJson(this.elasticsearchProperties));
        if (this.client == null) {
            List<Node> nodes = new ArrayList();
            log.info("(getClient)clusterEnable: {}", this.elasticsearchProperties.isClusterEnable());
            if (this.elasticsearchProperties.isClusterEnable()) {
                Iterator var5 = this.elasticsearchProperties.getHosts().iterator();

                while(var5.hasNext()) {
                    String host = (String)var5.next();
                    new Node(new HttpHost(host, this.elasticsearchProperties.getPort(), "http"));
                }
            } else {
                Node node = new Node(new HttpHost(this.elasticsearchProperties.getHost(), this.elasticsearchProperties.getPort(), "http"));
                nodes.add(node);
            }

            this.client = new RestHighLevelClient(RestClient.builder((Node[])nodes.toArray(new Node[nodes.size()])));
        }

        return this.client;
    }
}
