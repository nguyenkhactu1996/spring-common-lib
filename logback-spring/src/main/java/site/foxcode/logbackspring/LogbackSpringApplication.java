package site.foxcode.logbackspring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import site.foxcode.util.exception.ExceptionUtil;

@SpringBootApplication
@Slf4j
public class LogbackSpringApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(LogbackSpringApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        while (true){
            Thread.sleep(1000);
            try{
                int a = 1/0;
            }catch (Exception e){
                log.info("Exception: [{}]", ExceptionUtil.getFullStackTrace(e));
            }
//            log.info("command is running ...");
        }
    }
}
